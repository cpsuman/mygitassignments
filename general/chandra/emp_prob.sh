#!/bin/bash

/* Problem Statement: In a company which has multiple projects, and they are very strict about there daily syncup meetings.
They have introduced a fine system where if a person/person(s) gets late in joining the meeting they have to treat everyone
with a cup of coffee. The contribution of people will vary depending on by how much time they were late.
*/

read -p "Enter no of employee: " en
read -p "Enter cost of coffee: " cupprice
totalcost=$((en*cupprice))
echo "Total cost for coffee=$totalcost"

Totaldelay=0
initialtime=`date +%s`
declare -A EMPInTime
for ((i=0; i<en; i++))
do
        read -p "Provide Emp name: " name
        inTime=$((`date +%s` - initialtime))
        EMPInTime[$name]=$inTime
done

#Calculate delay in Employee Entry
for delay in "${EMPInTime[@]}"
do
        Totaldelay=$((Totaldelay + delay))
done
echo "Totaldelay = $Totaldelay"

delayFactor=$(echo "scale=2; $totalcost/$Totaldelay"|bc -l)
echo "delayFactor= $delayFactor"

#Calculate Employee contribution
for emp in "${!EMPInTime[@]}"
do
        if [ ${EMPInTime[$emp]} != 0 ]
        then
                contribution=$(echo "${EMPInTime[$emp]}*$delayFactor"|bc -l)
                echo "Contribution by $emp ---> $contribution"
        fi
done

