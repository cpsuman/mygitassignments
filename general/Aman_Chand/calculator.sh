#!/bin/bash
# perform input function on the 2 numbers

read -p "Please enter Ist number = " num1
read -p "Please enter 2nd number = " num2
echo -e "Options for performing the operations-: \n + for addition \n - for subtraction \n * for multiplication \n / for division"
read -p "Please enter the choice you want to perform = " perform
result=$(echo "scale=2;$num1$perform$num2" |bc)
echo "The result for the above operation is $result"
