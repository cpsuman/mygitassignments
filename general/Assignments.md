
# ASSIGNMENTS

  1. Create a shell script which will take your name as a input from the user and print "Welcome (Your Name)"
     eg: input : Rajat
         output: Welcome Rajat

  2. Create a shell script which will take 2 numbers as a input from the user and ask user actions(addition , subtraction , multiplication , division) to perform on these two numbers"
     eg: Please Enter two number:
         4 6
         What you want to perform:
         1. Addition
         2. Subtraction
         3. Multiplication
         4. Division
         3
         Result is 24
         
