#!/bin/bash

#shell script: take your name as an input and print "Welcome (Your Name)"
read -p "Please enter your name: " NAME
if ! [[ "$NAME" =~ ^[0-9]+$ ]]
then
	echo "Welcome ${NAME}"
else
	echo "Please input a string name !!"
	exit 1
fi
