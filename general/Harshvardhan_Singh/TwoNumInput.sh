#!/bin/bash

#script: Will take 2 numbers as a input and perform airthmatic operarions(addition , subtraction , multiplication , division) as per user choice

read -p "Please Enter two number: " x y
echo "X=$x Y=$y"
echo "What you want to perform:"
echo -e " 1. Addition\n 2. Subtraction\n 3. Multiplication\n 4. Divison"
read -p "Please select operation: " operation
echo "Operation $operation"
case $operation in
	1)
		echo "$x + $y =$(echo "scale=2; $x+$y"|bc)"
		;;
	2)
		echo "$x - $y =$(echo "scale=2; $x-$y"|bc)"
		;;
	3)
		echo "$x * $y =$(echo "scale=2; $x*$y"|bc)"
		;;
	4)
		echo "$x / $y =$(echo "scale=2; $x/$y"|bc)"
		;;
	*)
		echo "Please select valid choice; Exiting now"
		;;
	esac
