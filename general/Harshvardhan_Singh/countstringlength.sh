#!/bin/bash

#Usage: shell script, will take a string as an input from the user and print no of character in that string

read -p "Please provide string for which you want to get length:" string

echo "length of $string is: ${#string}"
