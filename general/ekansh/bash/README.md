# Script 1

```
#!/bin/bash
# Create a shell script which will take your name as a input from the user and print "Welcome (Your Name)"
#eg: input : Rajat
#output: Welcome Rajat

echo "Welcome $1"

```
Run " bash script1.sh Ekansh "