# ASSIGNMENTS

  1. Create a shell script which will take a string as a input from the user and print the no of character in that specific string
     
    eg: input : Rajat
        output: 5
   
  2. Create a shell script :
    
     * take input string1
     * take input string2
     * take input filepath 
     * replace first occurrence of string1 in each line of a given file with string2 

    eg : file_content : "Hi How r u Hi 
                         This is Opstree Hi Hi
                         Its in Noida Hi
                         Hi  "
         string1 = Hi
         string2 = Hello
         O/p : "Hello How r u Hi
                This is Opstree Hello Hi
                Its in Noida Hello  
                Hello  "
        
